import { Component, OnInit, OnChanges, SimpleChanges } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { AppService } from "../app.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";

// This interface may be useful in the times ahead...
export interface Member {
  id?: number;
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

@Component({
  selector: "app-member-details",
  templateUrl: "./member-details.component.html",
  styleUrls: ["./member-details.component.css"]
})
export class MemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  subscriptions: Subscription[] = [];
  isEdit: boolean;
  editId: number;

  constructor(
    private appService: AppService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isEdit = this.route.snapshot.url.toString().includes("edit");
    this.editId = this.isEdit ? parseInt(this.route.snapshot.paramMap.get('id')): 0;
    this.memberForm = new FormGroup({
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      jobTitle: new FormControl("", Validators.required),
      status: new FormControl("", Validators.required),
      team: new FormControl()
    });
    this.subscriptions.push(
      this.appService.getTeams().subscribe(y => {
        console.log(y);
        this.teams = [...y];
        this.memberForm.get('team').setValue(this.teams[0]);
        if (this.isEdit) {
          const id = this.route.snapshot.paramMap.get("id");
          this.subscriptions.push(
            this.appService.getMemberById(id).subscribe(y => {
              this.memberModel = y;
              const { id, ...model } = this.memberModel;
              this.memberForm.setValue(model);
              console.log(this.memberForm.value);
            })
          );
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('Changes!!!');
    console.log(changes)
  }

  // TODO: Add member to members
  onSubmit(form: FormGroup) {
    console.log('submit fired')

    this.memberModel = form.value;
    const addMember = this.isEdit ? {...this.memberModel, id: this.editId} : this.memberModel;
    console.log(addMember);
    this.appService.addMember(addMember).subscribe(e => {
      console.log(e);
      this.router.navigateByUrl('/members');
    });
  }
}
