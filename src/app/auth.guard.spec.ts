import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, {
        provide: HttpClient,
        useClass: class{
          
        }
      }]
    });
  });

  it('should initialize Guard', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  
});
