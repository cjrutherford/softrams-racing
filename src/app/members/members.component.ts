import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];
  modalOpen:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  selectedId: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(public appService: AppService, private router: Router) {}

  ngOnInit() {
    this.appService.getMembers().subscribe(members => (this.members = members));
  }

  ngOnDestroy(){
    this.modalOpen.unsubscribe();
    this.selectedId.unsubscribe();
  }

  goToAddMemberForm() {
    this.router.navigateByUrl('/addMember');
  }

  editMemberByID(id: number) {
    this.router.navigateByUrl(`/edit/${id}`);
  }

  deleteMemberById(id: number) {
    this.appService.deleteMember(id);
    this.members.splice(this.members.findIndex(x => x.id === id), 1);
  }
}
