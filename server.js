const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var hsts = require('hsts');
const path = require('path');
var xssFilter = require('x-xss-protection');
var nosniff = require('dont-sniff-mimetype');
const request = require('request');
const axios = require('axios');

const joi = require('@hapi/joi');

const app = express();

app.use(cors());
app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(xssFilter());
app.use(nosniff());
app.set('etag', false);
app.use(
  helmet({
    noCache: true
  })
);
app.use(
  hsts({
    maxAge: 15552000 // 180 days in seconds
  })
);

app.use(
  express.static(path.join(__dirname, 'dist/softrams-racing'), {
    etag: false
  })
);

app.get('/api/members', (req, res) => {
  request('http://localhost:3000/members', (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

app.delete('/api/members/:id', (req,res) => {
  const {params: {id}} = req;
  request(`http://localhost:3000/members/${id}`, {
    method: 'DELETE'
  }, (err, response, data) => {
    // console.log(response);
    res.send(true);
  })
})

app.get('/api/members/:id', (req, res) => {
  const {params:{id}} = req;
  request(`http://localhost:3000/members/${id}`, (err, response, body) => {
    if (response.statusCode <= 500) {
      const member = JSON.parse(body);

      res.send(member);
      // res.send(body.filter(m => m.id === req.params.id));
    }
  });
});



// TODO: Dropdown!
app.get('/api/teams', (req, res) => {
  request('http://localhost:3000/teams', (err, response, body) => {
    if(response.statusCode <= 500){
      body = JSON.parse(body);
      console.log(body);
      res.send(body);
    }
  })
});

// Submit Form!
app.post('/api/addMember', (req, res) => {
  const {body} = req;
  const expected = joi.object().keys({
    id: joi.number().optional(),
    firstName: joi.string().required(),
    lastName: joi.string().required(),
    jobTitle: joi.string().required(),
    status: joi.string().required().allow('Active', 'Inactive'),
    team: joi.string()
  });

  const {error, value} = expected.validate(body);
  if(error) res.error(error);
  console.log('value:')
  console.log(value);
  if(value.id){
    /**
     * According to the docs for json server this call should work,
     * but the output of json-server states PATCH /members 404 3.000 ms - 2
     */
    // console.log('patch')
    // axios.patch(`http://localhost:3000/members/${value.id}`, {
    //   ...value
    // }).then(response => {
    //   console.log(response.data);
    //   res.send(response.data);
    // }).catch(err => {
    //   console.error(err);
    //   res.error(err);
    // })
    request({
      url: 'http://localhost:3000/members',
      method: 'PUT',
      headers: {'Content-Type':'applciation/json'},
      body: {...value}
    }, (err, dbResponse, body) => {
      if(err) console.log(err)
      console.log(err)
      console.log(dbResponse);
      console.log(body)
      // console.log(dbResponse)
      res.send(body);
    })
  }else {
    console.log('post');
    // axios.post(`https://localhost:3000/members`, {
    //   ...value
    // }).then(response => {
    //   console.log(response.data);
    //   res.send(response.data);
    // }).catch(err => {
    //   console.error(err);
    //   res.error(err);
    // })
    request({
      url: 'http://localhost:3000/members',
      method: 'POST',
      headers: {'Content-Type':'applciation/json'},
      body: {...value}
    }, (err, response, body) => {
      console.log(err)
      console.log(response);
      body = JSON.parse(body);
      console.log(body)
      request(`http://localhost:3000/members/${body.id}`, (err, getResponse, getBody) => {
        res.send(getBody);
      })
    });
  }
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/softrams-racing/index.html'));
});

app.listen('8000', () => {
  console.log('Vrrrum Vrrrum! Server starting!');
});
